package com.Viktoriia;

public class Stdin_Stdout {
    public static void main(String args[]) {

        String hello = "String: ";
        String name = "Welcome to HackerRank's Java tutorials!";
        System.out.println(hello + name);
    }

    {
        String hey = "Double: ";
        String name = "3.1415";
        System.out.println(hey + name);
    }

    {
        String hello = "Int: ";
        String name = "42";
        System.out.println(hello + name);
    }
}