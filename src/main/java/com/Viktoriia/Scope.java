package com.Viktoriia;

public class Scope {

    // because x is declared outside of any method, it is in scope to all methods, in other words
    // any method can access x
    static int x;

    public static void main(String args[]){
        x = 5;
        doSomething();
        System.out.println(x);
    }

    static void doSomething() {
        x = 10;
    }

    static void doSomethinglocally() {
        // because y is declared inside of this method, it is local to this method, in other words
        // no other method has access to y
        int y = 100;
    }
}
