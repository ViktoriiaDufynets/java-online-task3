package com.Viktoriia;

public class Application {
    public static void main(String args[]){
        int n = 3;

        if( n%2 == 1 ){
            System.out.print("Weird");
        }else if( (n%2 == 0) && (n >= 2) && (n <= 5) ){
            System.out.print("Not Weird");
        }else if( (n%2 == 0) && (n >= 6) && (n <= 20) ){
            System.out.print("Weird");
        }else if( (n%2 == 0) && (n >= 20) ){
            System.out.print("Not Weird");
        }
    }
}