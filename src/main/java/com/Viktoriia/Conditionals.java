package com.Viktoriia;

public class Conditionals {
    public static void main(String args[]){

        // if (some expression that evaluate to true)
        //     do something

        boolean sayHello = false;
        boolean sayHey = true;

        // only the first statement to evaluate to tree gets executed
        if(sayHello) {
            System.out.println("Hello");
        } else if(sayHey) {
            System.out.println("Hey");
        } else {
            System.out.println("Goodbye");

        // comments what we can use in conditions: >, <, >=, <=, ! - not equal, != - not equal to.., ==
        // && AND, || OR
        }
    }
}
