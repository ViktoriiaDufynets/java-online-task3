package com.task3_Shape;

public class Shape {

    private int squareOfShape;
    private String colour;
    private int amount = 3;

    public void glow (int size) {
        if (size > 30) {
            System.out.println("glow shiny");
        } else if (size > 20) {
            System.out.println("glow dimly");
        } else {
            System.out.println("doesn't glow");
        }
    }

    public int getSquareOfShape() {
        return squareOfShape;
    }

    public void setSquareOfShape(int squareOfShape) {
        this.squareOfShape = squareOfShape;
    }

    public int getColour() {
        return colour;
    }

    public void setColour(int colour) {
        this.colour = colour;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = 3;  // question
    }
}
