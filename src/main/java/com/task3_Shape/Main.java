package com.task3_Shape;

public class Main {
    public static void main(String args[]) {
        Circle ring = new Circle();
        Triangle masson = new Triangle();
        Rectangle symbol = new Rectangle();

        ring.setSquareOfShape(11);
        System.out.println(ring.getSquareOfShape());
        ring.glow(29);
        ring.colour(ring.getColour());
    }
}
