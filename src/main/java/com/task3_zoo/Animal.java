package com.task3_zoo;

public class Animal {

    private int sizeOfAnimal;
    private int eyes;
    private boolean heart = true;

    public void sleep(int size) {
        if (size > 60) {
            System.out.println("Sleep on tree");
        } else if (size > 20) {
            System.out.println("Sleep in the burrow");
        } else {
            System.out.println("Sleep in the water");
        }
    }
    public void eatAnything(int size) {
        if (size > 60) {
            System.out.println("I am predator");
        } else if (size > 15) {
            System.out.println("I am vegan");
        } else {
            System.out.println("I am weak");
        }
    }
    public int getSizeOfAnimal() {
        return sizeOfAnimal;
    }

    public void setSizeOfAnimal(int sizeOfAnimal) {
        this.sizeOfAnimal = sizeOfAnimal;
    }

    public int getEyes() {
        return eyes;
    }

    public void setEyes(int eyes) {
        this.eyes = eyes;
    }

    public boolean isHeart() {
        return heart;
    }

    public void setHeart(boolean heart) {
        this.heart = heart;
    }
}