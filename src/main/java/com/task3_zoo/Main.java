package com.task3_zoo;

public class Main {
    public static void main(String args[]) {
        Fish morena = new Fish(); //morena - is an object
        Parrot zyablik = new Parrot();
        Eagle hawk = new Eagle();

        morena.setSizeOfAnimal(11);
        System.out.println(morena.getSizeOfAnimal());
        morena.swim();
        morena.sleep(morena.getSizeOfAnimal());
        morena.eatAnything(morena.getSizeOfAnimal());
        System.out.println(morena.tail);


        zyablik.setSizeOfAnimal(21);
        System.out.println(zyablik.getSizeOfAnimal());
        zyablik.fly();
        zyablik.sleep(zyablik.getSizeOfAnimal());
        zyablik.eatAnything(zyablik.getSizeOfAnimal());
        zyablik.eatPlants();

        hawk.setSizeOfAnimal(65);
        System.out.println(hawk.getSizeOfAnimal());
        hawk.fly();
        hawk.sleep(hawk.getSizeOfAnimal());
        hawk.eatAnything(hawk.getSizeOfAnimal());
        hawk.eatMeat();
        hawk.chaseMouse();
    }
}
